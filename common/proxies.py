import os
import sys
from copy import deepcopy
from pathlib import Path
import yaml
import requests
from typing import Any, Dict, List, Optional, Tuple, Union, Callable

try:
    from typing import GenericMeta  # python 3.6
except ImportError:
    # in 3.7, GenericMeta doesn't exist but we don't need it
    class GenericMeta:  # noqa: WPS440 (block variable overlap)
        """Placeholder."""

if __name__ == '__main__':
    sys.path.append(os.path.abspath(os.path.join(Path(__file__).parent, os.path.pardir)))
sys.path.append(os.path.abspath(os.path.join(Path(__file__).parent, os.path.pardir)))
from applib.conf_lib import getConf
from applib.log_lib import app_log
info, debug, warn, error = app_log.info, app_log.debug, app_log.warning, app_log.error

class Proxies:

    def __init__(self, struct: Dict = {}, conf_path='./config.yaml'):
        self.conf_path = os.path.abspath(conf_path)
        self.provider = getConf(self.conf_path, root_key='proxies_provider')
        self._struct = struct or {}

    def get_struct(self) -> dict:
        """
        Return a copy of the struct dictionary of this Proxies object.
        Returns:
            A copy of the struct dictionary.
        """
        return deepcopy(self._struct)

    def get(self, item: str, class_: Union[GenericMeta, Callable] = None):
        value = self._struct.get(item)
        if class_ is not None and value is not None:
            return class_(value)

    def set(self, key: str, value: Any):
        self._struct[key] = value

    @property
    def proxies(self) -> Dict:
        proxies = self.get('proxies')
        if not proxies:
            data = yaml.load(
                requests.get(self.provider).text, Loader=yaml.Loader)

            self.set('proxies', data['proxies'])
            return data['proxies']

        return proxies

    # @proxies.setter
    # def proxies(self, value):
    #     self.set("proxies", value)

if __name__ == '__main__':
    proxies = Proxies()
    print(proxies.proxies)