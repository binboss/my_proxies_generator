import os
import sys
import requests
from urllib.parse import quote
import yaml
from typing import IO, Any, BinaryIO, Dict, List, Optional, Tuple, Union
from pathlib import Path
if __name__ == '__main__':
    sys.path.append(os.path.abspath(os.path.join(Path(__file__).parent.parent, os.path.pardir)))

sys.path.append(os.path.abspath(os.path.join(Path(__file__).parent.parent, os.path.pardir)))
from common.proxies import Proxies
from applib.conf_lib import getConf
from applib.log_lib import app_log
info, debug, warn, error = app_log.info, app_log.debug, app_log.warning, app_log.error

class Instance(object):

    def __init__(self, conf_path='./config.yaml'):
        self.conf_path = os.path.abspath(conf_path)
        self.conf = getConf(self.conf_path)
        self.coalcloud_conf = self.conf['provider']['coalcloud']
        self.headers = {
            'authority': 'cdn.coalcloud.net',
            'accept': 'application/json, text/javascript, */*; q=0.01',
            'dnt': '1',
            'x-requested-with': 'XMLHttpRequest',
            'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36',
            'sec-fetch-site': 'same-origin',
            'sec-fetch-mode': 'cors',
            'sec-fetch-dest': 'empty',
            'referer': 'https://cdn.coalcloud.net/',
            'accept-language': 'zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7,zh-TW;q=0.6,ja;q=0.5',
            'cookie': self.coalcloud_conf['cookie']
        }

    def get_serverlist(self) -> Dict:
        url = "https://cdn.coalcloud.net/serverlist"

        payload={}

        response = requests.request("GET", url, headers=self.headers, data=payload)

        return response.json()

    def get_conectionlist(self) -> Dict:

        url = "https://cdn.coalcloud.net/connectionsList"

        payload='pageSize=20&pageNum=1&orderByColumn=&isAsc=asc'

        response = requests.request("POST", url, headers=self.headers, data=payload)

        return response.json()

    def create(self, name: str, sid: int, dip: str, dport: int) -> Dict:

        url = "https://cdn.coalcloud.net/createProxyConnection"
        headers = dict(self.headers)
        headers['content-type'] = 'application/x-www-form-urlencoded; charset=UTF-8'

        payload=f"name={quote(name, safe='/',)}&sid={sid}&sport=&dip={dip}&dport={dport}"

        response = requests.request("POST", url, headers=headers, data=payload)

        return response.json()

    def delete(self, id: int) -> Dict:

        url = f"https://cdn.coalcloud.net/deleteOnConnections?connectionsId={id}"

        payload={}

        response = requests.request("GET", url, headers=self.headers, data=payload)

        return response.json()

    def get_dest_clash_proxies(self) -> List:
        dest_proxies = self.coalcloud_conf['dest_proxies']
        clash_proxies = []
        for proxy in Proxies().proxies:
            if proxy['name'] in dest_proxies:
                clash_proxies.append(proxy)

        return clash_proxies

    def get_proxies(self) -> str:
        clash_proxies = self.get_dest_clash_proxies()
        serverlist = self.get_serverlist()
        connectionlist = self.get_conectionlist()
        # clean
        avaliable_server_ids = [server['id'] for server in serverlist['rows']]
        for connection in connectionlist['rows']:
            sid = connection['sid']
            if sid not in avaliable_server_ids or sid in self.coalcloud_conf['disabled_servers']:
                self.delete(connection['id'])

        # create
        for server in serverlist['rows']:
            sid = server['id']
            sname = server['name']

            if '不可用' in sname:
                continue

            if int(sid) in self.coalcloud_conf['disabled_servers']:
                info(f"[provider.coalcloud] \"{sname} (id: {sid})\" is not in the creating list, ignore.")
                continue

            info(f"[provider.coalcloud] Creating coalcloud server: {sname} (id: {sid})")
            sname = sname.split(' ')[0]
            
            for index, proxy in enumerate(clash_proxies):
                dip = proxy['server']
                dport = proxy['port']
                dname = proxy['name']
                name = f"{index + 1}-{sname}->{dname}"
                res = self.create(name, sid, dip, dport)
                warn(res['msg'])

        # create clash proxies
        clash_coalcloud_proxies = []
        connectionlist = self.get_conectionlist()
        proxies_src = Proxies().proxies

        # store nested proxies to avoid duplicated iterations
        if self.coalcloud_conf['generate_nested_proxies']['enable']:
            nested_proxies_map = {}

        for connection in connectionlist['rows']:
            cid = int(connection['name'].split('-')[0])
            node_name = connection['name'].split('->')[0]

            proxy = clash_proxies[cid - 1]
            d_proxy = dict(proxy)
            d_proxy['name'] = f"coalcloud-{node_name}->{proxy['name']}"
            d_proxy['server'] = connection['sip']
            d_proxy['port'] = connection['sport']

            clash_coalcloud_proxies.append(d_proxy)

            if self.coalcloud_conf['generate_nested_proxies']['enable']:                
                nested_proxies = nested_proxies_map.get(proxy['server'], None)

                # load live nested proxies
                if nested_proxies is None:
                    nested_proxies = []
                    proxy_src_index = 0
                    while proxy_src_index < len(proxies_src):
                        proxy_src = proxies_src[proxy_src_index]
                        if proxy_src['server'] == proxy['server'] and proxy_src['name'] != proxy['name']:
                            nested_proxies.append(proxy_src)
                            proxies_src.pop(proxy_src_index)
                            continue
                        
                        proxy_src_index += 1
                        

                    nested_proxies_map[proxy['server']] = nested_proxies

                for nested_proxy in nested_proxies:
                    if self.coalcloud_conf['generate_nested_proxies']['match_type'] and nested_proxy['type'] != proxy['type']:
                        continue

                    d_proxy = dict(nested_proxy)
                    d_proxy['name'] = f"coalcloud-{node_name}->{nested_proxy['name']}"
                    d_proxy['server'] = connection['sip']
                    d_proxy['port'] = connection['sport']

                    clash_coalcloud_proxies.append(d_proxy)

        return clash_coalcloud_proxies

if __name__ == "__main__":
    instance = Instance()
    print(instance.get_proxies())