import os
import sys
import requests
from typing import IO, Any, BinaryIO, Dict, List, Optional, Tuple, Union
from pathlib import Path
if __name__ == '__main__':
    sys.path.append(os.path.abspath(os.path.join(Path(__file__).parent.parent, os.path.pardir)))
sys.path.append(os.path.abspath(os.path.join(Path(__file__).parent.parent, os.path.pardir)))

from common.proxies import Proxies
from applib.conf_lib import getConf
from applib.log_lib import app_log
info, debug, warn, error = app_log.info, app_log.debug, app_log.warning, app_log.error

class Instance(object):
    def __init__(self, conf_path='./config.yaml'):
        self.conf_path = os.path.abspath(conf_path)
        self.conf = getConf(self.conf_path)
        self.cloudflare_conf = self.conf['provider']['cloudflare']

    def get_dest_clash_proxies(self) -> List:
        dest_proxies = self.cloudflare_conf['dest_proxies']
        clash_proxies = []
        for proxy in Proxies().proxies:
            if proxy['name'] in dest_proxies:
                clash_proxies.append(proxy)

        return clash_proxies

    def get_proxies(self) -> Dict:
        if 'url' in self.cloudflare_conf:
            res = requests.get(self.cloudflare_conf['url'])
            cloudflare_ip = res.text.strip()

            clash_cloudflare_proxies = []
            clash_proxies = self.get_dest_clash_proxies()
            for proxy in clash_proxies:
                proxy['name'] = f"cloudflare->{proxy['name']}"
                proxy['server'] = cloudflare_ip
                clash_cloudflare_proxies.append(proxy)

            return clash_cloudflare_proxies

if __name__ == "__main__":
    instance = Instance()
    print(instance.get_proxies())