import os
import requests
import json
import yaml
from typing import IO, Any, BinaryIO, Dict, List, Optional, Tuple, Union
from datetime import datetime
from pathlib import Path
import provider
import storage
from applib.conf_lib import getConf
from applib.tools_lib import pcformat
from applib.submoduleloader_lib import import_submodules
from applib.log_lib import app_log
info, debug, warn, error = app_log.info, app_log.debug, app_log.warning, app_log.error

import_submodules(provider)
import_submodules(storage)

class ProxiesManager():
    
    def __init__(self):
        self.work_dir = Path(__file__).parent
        conf_file_path = self.work_dir / 'config.yaml'
        conf = getConf(os.fspath(conf_file_path))
        conf['conf_file_path'] = conf_file_path

        self.all_conf = conf
        self.conf_file_path = self.all_conf['conf_file_path']

    def log(self):
        work_dir = Path(__file__).parent
        with open(work_dir / 'log.log', 'w') as outf:
            outf.write(datetime.now().isoformat())

    def main(self):
        clash_proxies_collection = []
        # providers_updated = []

        for provider_name, provider_conf in self.all_conf['provider'].items():
            if not provider_conf['enable']:
                continue

            info(f'[provider.{provider_name}] Getting proxies...')
            provider_instance = getattr(getattr(provider, provider_name), provider_name).Instance()
            clash_proxies = provider_instance.get_proxies()
            if clash_proxies:
                clash_proxies_collection += clash_proxies
                # providers_updated.append(provider_name)

        # initial storage instance
        storage_instance_object = {}
        for storage_name in self.all_conf['storage'].keys():
            info(f'[storage.{storage_name}] Updating {storage_name}...')
            storage_instance = getattr(getattr(storage, storage_name), storage_name).Instance()
            storage_instance_object[storage_name] = storage_instance

        # clear and store
        for storage_name, storage_instance in storage_instance_object.items():
            clash_curr_proxies = yaml.load(storage_instance.get_current(), Loader=yaml.Loader)
            for provider_name in self.all_conf['provider'].keys():
                clash_curr_proxies['proxies'] = [proxy for proxy in clash_curr_proxies['proxies'] if provider_name not in proxy['name']]

            clash_curr_proxies['proxies'] += clash_proxies_collection
            clash_curr_proxies = yaml.dump(clash_curr_proxies, allow_unicode=True, sort_keys=False)

            storage_instance.update(clash_curr_proxies)

        # log
        self.log()

        info('done')

if __name__ == "__main__":
    proxies_manager = ProxiesManager()
    proxies_manager.main()