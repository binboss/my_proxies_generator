import os
import sys
import github
from typing import IO, Any, BinaryIO, Dict, List, Optional, Tuple, Union
from pathlib import Path
if __name__ == '__main__':
    sys.path.append(os.path.abspath(os.path.join(Path(__file__).parent.parent, os.path.pardir)))

from applib.conf_lib import getConf
from applib.log_lib import app_log
info, debug, warn, error = app_log.info, app_log.debug, app_log.warning, app_log.error

class Instance(object):

    def __init__(self, conf_path='./config.yaml'):
        self.conf_path = os.path.abspath(conf_path)
        self.conf = getConf(self.conf_path, root_key='storage')
        self.conf = self.conf['gist']

        self.gist_id = self.conf['id']
        self.gh = github.Github(self.conf['token'])
        self.gist = self.gh.get_gist(self.gist_id)
        self.gist_filename = self.conf['filename']

    def get_gist_content(self):
        for fname, gistfile in self.gist.files.items():
            if fname != self.gist_filename:
                continue

            return gistfile.content

    def update_gist(self, content: str):        
        
        self.gist.edit(
            files={self.gist_filename: github.InputFileContent(content=content)},
        )

    def get_current(self):
        return self.get_gist_content()

    def update(self, content):
        self.update_gist(content)